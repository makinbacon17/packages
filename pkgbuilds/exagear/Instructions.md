# Exagear instructions

- manually extract the guest image to `/opt/exagear/images/centos-7-x86_64/`
- run `IMAGE_DIR_NAME=centos-7-x86_64 /opt/exagear/images/centos-7-x86_64/.exagear/create-vpaths-list-stubs.sh`
- `export EXAGEAR_GUEST_IMAGE_NAME=centos-7`
- `export EXAGEAR_GUEST_ARCH_NAME=x86_64`
- Run `exagear`
