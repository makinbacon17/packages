# Switch PKGBUILDS

## Building packages

Build the dockerfile :
```
docker build -t l4t-arch -f dockerfiles/Dockerfile.arch dockerfiles/
```

Run a container using the docker image you created above :
```
docker build --rm -it -v $(pwd)/pkgbuilds/<package> l4t-arch # replace <package> by the package name
```

## Contributors

Many thanks to @benjaxd, @ave, @Stary2001
